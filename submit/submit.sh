#!/bin/bash

cd /lustre/collider/zhuyifan/DiHiggs/pulls
echo "ulimit -S -s unlimited; quickFit -f workspaces/Hcomb_natureSTXS_0409_diff_1kEW_asimov.root -p k3=1_-100_100,kt=1_-100_100 -o workspaces/Hcomb_natureSTXS_0409_diff_1kEW_asimov_cookWS.root --minTolerance 1e-4 --hesse 1 --saveWS 1 --saveNP 1 -k SnapShotFullFloat -s conditionalGlobs_1 -d asimovData_1 2>&1" | /cvmfs/atlas.cern.ch/repo/containers/sw/singularity/x86_64-el7/current/bin/singularity exec -B $PWD ../analyzer_2.2.sif bash 2>&1
