|workspace|name|MD5|
|----|----|----|
|HH+H 84 correlated|HH\_H\_correlated\_cookWS\_fix\_0510.root|a8615754975ad4c7de32e88c078aad44|
|HH+H 0 correlated|HH\_H\_cookWS\_0430.root|7af8866ba02f2ca115673ad2ef74d377|
|HH+H 53 correlated|HH\_H\_correlated\_53\_cookWS\_0512.root|99b7ef58e0e362c63ad2d9cec4953dcc|
|Single H|SH\_cookWS\_0503\_fix.root|7a4c53e0f2dd4558a5200a853982940a|
|HH|HH\_0412\_obs\_cookWS.root|3bb922248d50600e00a05a540d06b8e6|

# fit results for POI
|POI|H|HH|HH+H 0 correlated | HH+H (53 corr + 31 decorr) | HH+H (84 corr) |
|----|----|----|----|----|----|
|$\kappa_{\lambda}$|2.624 ± 5.143 |1.28 ± 4.32 |2.956 ± 1.942 |2.963 ± 1.930 |2.974 ± 1.885 |
|$\kappa_{\mathrm{t}}$|0.996 ± 0.049 |0.30 ± 1.23|0.998 ± 0.044 |0.997 ± 0.044 | 1.000 ± 0.044|

# NP pulls and constrain
All 3 correlation scheme compared, as shown as each row in the legends
1. HH+H 0 correlated: all NP decorrelated between HH and H in the combination
2. HH+H (84 corr): 84 NP orrelated between HH and H in the combination
3. HH+H (53 corr + 31 decorr): 31 NP (among the 84 terms) decorrelated between HH and H in the combination

<img src="1_to_6.png" width="600"/>
<img src="79_to_84.png" width="600"/>
<img src="electrons.png" width="600"/>
<img src="jets_1.png" width="600"/>
<img src="jets_2.png" width="600"/>
<img src="jets_3.png" width="600"/>
<img src="muons.png" width="600"/>
<img src="taus.png" width="600"/>
