|workspace|name|MD5|
|----|----|----|

# fit results for POI
|POI| HH+H simple | HH+H uncorr |
|----|----|----|

# NP pulls and constrain
All 3 correlation scheme compared, as shown as each row in the legends

## Round2 no boosted bb
<img src="1_to_6.png" width="600"/>
<img src="80_to_90.png" width="600"/>
<img src="electrons.png" width="600"/>
<img src="jets_1.png" width="600"/>
<img src="jets_2.png" width="600"/>
<img src="jets_3.png" width="600"/>
<img src="muons.png" width="600"/>
<img src="taus.png" width="600"/>

## Round2 no boosted bb, check simple scheme
<img src="simple_scheme.png" width="600"/>
